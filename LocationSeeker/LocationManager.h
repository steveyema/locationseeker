//
//  LocationManager.h
//  LocationPeeker
//
//  Created by Steve Ma on 2014-12-31.
//  Copyright (c) 2014 Steve Ma. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

#define kSIGNIFICANT_LOCATION_TIME_DIFFERENTIAL 30

@protocol LocationManagerDelegate <NSObject>

- (void)locationUpdated:(BOOL)failed;

@end

@interface LocationManager : NSObject<CLLocationManagerDelegate>

@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) CLLocation *lastKnowLocation;
@property (nonatomic, strong) NSArray *lastKnowPlacemarks;
@property (nonatomic, strong) NSDate *lastKnownLocationTime;

- (void) startLocationUpdateForObserver:(id<LocationManagerDelegate>)observer;
- (void)stopLocationUpdateForObserver:(id<LocationManagerDelegate>)observer;
@property (NS_NONATOMIC_IOSONLY, readonly) BOOL lastLocationTooOld;
+ (LocationManager *)sharedInstance;

@end
