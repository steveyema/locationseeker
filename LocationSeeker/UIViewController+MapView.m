//
//  UIViewController+MapView.m
//  LocateMe
//
//  Created by Ye Ma on 2015-06-01.
//  Copyright (c) 2015 Ye Ma. All rights reserved.
//

#import "UIViewController+MapView.h"
#import "LibraryAPI.h"
#import "NSManagedObject+Place.h"

@implementation UIViewController (MapView)

-(MKAnnotationView *)mapView:(MKMapView *)mapView
           viewForAnnotation:(id<MKAnnotation>)annotation
{
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    
    static NSString *reuseId = @"reuseid";
    MKAnnotationView *av = [mapView dequeueReusableAnnotationViewWithIdentifier:reuseId];
    if (av == nil)
    {
        av = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:reuseId];
        [av.layer setCornerRadius:5];
        [av.layer setShadowColor:[UIColor blackColor].CGColor];
        [av.layer setShadowOpacity:1.0f];
        [av.layer setShadowRadius:5.0f];
        [av.layer setShadowOffset:CGSizeMake(0, 0)];
        [av setBackgroundColor:[UIColor grayColor]];
        
        UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
        //        lbl.backgroundColor = [UIColor blackColor];
        [lbl.layer setCornerRadius:5];
        lbl.textAlignment = NSTextAlignmentCenter;
        lbl.textColor = [UIColor whiteColor];
        lbl.alpha = 0.5;
        lbl.tag = 42;
        [av addSubview:lbl];
        
        //Following lets the callout still work if you tap on the label...
        av.canShowCallout = YES;
        av.frame = lbl.frame;
    }
    else
    {
        av.annotation = annotation;
    }
    
    UILabel *lbl = (UILabel *)[av viewWithTag:42];
    if (annotation.title && annotation.title.length > 0) {
        NSString *firstLetter = [annotation.title substringToIndex:1];
        lbl.text = [firstLetter uppercaseString];
    }
    
    return av;
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    NSLog(@"select pin");
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(calloutTapped:)];
    [view addGestureRecognizer:tapGesture];
}

-(void)calloutTapped:(UITapGestureRecognizer *) sender
{
    NSLog(@"Callout was tapped");
    
    MKAnnotationView *view = (MKAnnotationView*)sender.view;
    NSManagedObject *annotation = [view annotation];
    if (annotation != nil)
    {
        NSLog(@"%f %f", [annotation coordinate].latitude, [annotation coordinate].longitude);
        [self openMapsWithDirectionsTo:annotation.coordinate];
    }
}

- (void)openMapsWithDirectionsTo:(CLLocationCoordinate2D)to {
    Class itemClass = [MKMapItem class];
    if (false && itemClass && [itemClass respondsToSelector:@selector(openMapsWithItems:launchOptions:)]) {
        MKMapItem *currentLocation = [MKMapItem mapItemForCurrentLocation];
        MKMapItem *toLocation = [[MKMapItem alloc] initWithPlacemark:[[MKPlacemark alloc] initWithCoordinate:to addressDictionary:nil]];
        toLocation.name = @"Destination";
        [MKMapItem openMapsWithItems:@[currentLocation, toLocation]
                       launchOptions:@{MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving, MKLaunchOptionsShowsTrafficKey: @YES}];
    } else {
        NSMutableString *mapURL = [NSMutableString stringWithString:@"http://maps.google.com/maps?"];
        [mapURL appendFormat:@"saddr=Current Location"];
        [mapURL appendFormat:@"&daddr=%f,%f", to.latitude, to.longitude];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[mapURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
    }
}


@end
