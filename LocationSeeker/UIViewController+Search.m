//
//  UIViewController+Search.m
//  LocateMe
//
//  Created by Ye Ma on 2015-06-04.
//  Copyright (c) 2015 Ye Ma. All rights reserved.
//

#import "UIViewController+Search.h"
#import "LibraryAPI.h"

@implementation UIViewController (Search)

- (void)reloadData
{
    //overwrite by subclass
}

- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event
{
    if (motion == UIEventSubtypeMotionShake)
    {
        if ([[LibraryAPI sharedInstance] isPredicateSearch]) {
            [[LibraryAPI sharedInstance] setPredicateCondition:nil];
            [self reloadData];
        }
        else {
            [self showAlert];
        }
    }
}

-(void)showAlert
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Search in Locations"
                                                        message:@"Enter a phrase for the name"
                                                       delegate:self
                                              cancelButtonTitle:@"Cancel"
                                              otherButtonTitles:@"Search", nil];
    alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
    
    [alertView show];
    
}

#pragma mark - UIAlertView delegate method

- (void)didPresentAlertView:(UIAlertView *)alertView
{
    UITextField * alertTextField = [alertView textFieldAtIndex:0];
    [alertTextField becomeFirstResponder];
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        UITextField * alertTextField = [alertView textFieldAtIndex:0];
        NSLog(@"alerttextfiled - %@",alertTextField.text);
        
        if (alertTextField.text && ![alertTextField.text isEqualToString:@""]) {
            NSPredicate *pred = [NSPredicate predicateWithFormat:@"placemark contains[cd] %@", alertTextField.text];
            [[LibraryAPI sharedInstance] setPredicateCondition:pred];
            [self reloadData];
        }
    }
}


@end
