//
//  viewControllerViewController.m
//  LocationSeeker
//
//  Created by Ye Ma on 2015-01-02.
//  Copyright (c) 2015 Ye Ma. All rights reserved.
//

#import "EditViewController.h"
#import "AppDelegate.h"
#import "LibraryAPI.h"
#import "NSManagedObject+Place.h"
#import "UIViewController+MapView.h"

@interface EditViewController ()
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@end

@implementation EditViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.textView.text = self.obj.address;
    
    self.mapView.delegate = self;
    [self.mapView showAnnotations:@[self.obj] animated:YES];
}

- (void)viewDidDisappear:(BOOL)animated
{
    if (![self.textView.text isEqualToString:self.obj.address]) {
        self.obj.address = self.textView.text;
        if ([self.delegate respondsToSelector:@selector(contentUpdated)]) {
            [self.delegate contentUpdated];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
