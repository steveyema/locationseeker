//
//  UIViewController+MapView.h
//  LocateMe
//
//  Created by Ye Ma on 2015-06-01.
//  Copyright (c) 2015 Ye Ma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface UIViewController (MapView)<MKMapViewDelegate>

@end
