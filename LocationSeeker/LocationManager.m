//
//  LocationManager.m
//  LocationPeeker
//
//  Created by Steve Ma on 2014-12-31.
//  Copyright (c) 2014 Steve Ma. All rights reserved.
//

#import "LocationManager.h"

#define timeExpireLimit    kSIGNIFICANT_LOCATION_TIME_DIFFERENTIAL

@interface LocationManager(){
    BOOL geocoding;
}

@property (nonatomic, assign) BOOL waitingForInUseAuthorizationResponse;
@property (nonatomic, strong) NSMutableArray *observers;
@property (nonatomic, strong) CLGeocoder *geocoder;

@end

@implementation LocationManager

- (instancetype)init
{
    if (self = [super init]) {
        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.delegate = self;
        self.observers = [NSMutableArray new];
    }
    return self;
}

+ (BOOL) mustPreRequestLocationPermission {
    if ([[LocationManager sharedInstance].locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        return ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined);
    }
    
    return NO;
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    
    if (self.waitingForInUseAuthorizationResponse) {
        if (status != kCLAuthorizationStatusNotDetermined) {
            self.waitingForInUseAuthorizationResponse = NO;
            [self startLocationUpdates];
        }
    }
    
}

- (void) requestWhenInUseAuthorization {
#if __IPHONE_8_0
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [self.locationManager requestWhenInUseAuthorization];
    }
#endif
}

- (void) startLocationUpdateForObserver:(id<LocationManagerDelegate>)observer
{
    if (![self.observers containsObject:observer]) {
        NSLog(@"Location Observer Added: %@", observer);
        [self.observers addObject:observer];
    }
    
    [self startLocationUpdates];
}

- (void)stopLocationUpdateForObserver:(id<LocationManagerDelegate>)observer
{
    if ([self.observers containsObject:observer]) {
        NSLog(@"Location Observer Removed: %@", observer);
        [self.observers removeObject:observer];
    }
    [self.locationManager stopUpdatingLocation];
}

- (void) startLocationUpdates
{
    if ([[self class] mustPreRequestLocationPermission] && !self.locationAuthorizationRequested) {
        self.waitingForInUseAuthorizationResponse = YES;
        [self requestWhenInUseAuthorization];
        return;
    }
    
    [self startUpdates];
    
}

- (void)startUpdates
{
    NSDate *now = [NSDate date];
    
    NSTimeInterval interval = (self.lastKnownLocationTime == nil)? kSIGNIFICANT_LOCATION_TIME_DIFFERENTIAL : [now timeIntervalSinceDate:self.lastKnownLocationTime];
    
    if (fabs(interval) >= kSIGNIFICANT_LOCATION_TIME_DIFFERENTIAL)
    {
        [self.locationManager startUpdatingLocation];
        NSLog(@"updating location...");
    }
}

- (BOOL) locationAuthorizationRequested {
    return [CLLocationManager authorizationStatus] != kCLAuthorizationStatusNotDetermined;
}

- (void)geocodeLocation
{
    if (geocoding) {
        return;
    }
    
    if (!self.geocoder)
        self.geocoder = [[CLGeocoder alloc] init];
    
    geocoding = YES;
    NSLog(@"geo coding...");
    [self.geocoder reverseGeocodeLocation:self.lastKnowLocation completionHandler:
     ^(NSArray* placemarks, NSError* error){
         geocoding = NO;
         self.lastKnowPlacemarks = placemarks;
         //retain copy of self.observers to keep consistency.
         NSArray *observers = [NSArray arrayWithArray:self.observers];
         if (observers.count > 0) {
             NSEnumerator *enumerator = [observers objectEnumerator];
             id object;
             while (object = [enumerator nextObject]) {
                 if ([object respondsToSelector:@selector(locationUpdated:)]) {
                     [object performSelector:@selector(locationUpdated:) withObject:@(YES)];
                 }
             }
         }
     }];
}

- (BOOL)lastLocationTooOld
{
    NSDate *now = [NSDate date];
    
    NSTimeInterval interval = [now timeIntervalSinceDate:self.lastKnownLocationTime];
    
    if (fabs(interval) < timeExpireLimit) {
        return NO;
    }
    
    return YES;
}


#pragma mark - Location Manager Delegate

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"location udpated: %f %f", newLocation.coordinate.latitude, newLocation.coordinate.longitude);
    self.lastKnowLocation = newLocation;
    self.lastKnownLocationTime = [[newLocation timestamp] copy];
    
    [self geocodeLocation];
    
//    [self.locationManager stopUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    NSLog(@"%@, %@", error, error.userInfo);
    if (self.observers.count > 0) {
        NSArray *observers = [NSArray arrayWithArray:self.observers];
        NSEnumerator *rator = [observers objectEnumerator];
        id object;
        while (object = [rator nextObject]) {
            if ([object respondsToSelector:@selector(locationUpdated:)]) {
                [object performSelector:@selector(locationUpdated:) withObject:@(NO)];
            }
        }
    }
//    [self.locationManager stopUpdatingLocation];
}

+ (LocationManager *)sharedInstance
{
    static LocationManager *_sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [LocationManager new];
    });
    return _sharedInstance;
}

@end
