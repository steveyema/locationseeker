//
//  viewControllerViewController.h
//  LocationSeeker
//
//  Created by Ye Ma on 2015-01-02.
//  Copyright (c) 2015 Ye Ma. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ViewControllerDelegate <NSObject>

-(void)contentUpdated;

@end

@interface EditViewController : UIViewController

@property (nonatomic, weak) id<ViewControllerDelegate> delegate;
@property (nonatomic, weak) NSManagedObject *obj;

@end
