//
//  PersistencyManager.h
//  LocateMe
//
//  Created by Ye Ma on 2015-06-01.
//  Copyright (c) 2015 Ye Ma. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PersistencyManager : NSObject

@property (nonatomic, strong, readonly) NSMutableArray *managedObjects;
@property (nonatomic, strong, readonly) NSMutableArray *managedObjectsNearby;
@property (nonatomic, strong, readwrite) NSPredicate *predicate;
@property (nonatomic, assign, readonly, getter=isPredicateSearch) BOOL predicateSearch;

@property (NS_NONATOMIC_IOSONLY, readonly) BOOL addPlace;
@property (NS_NONATOMIC_IOSONLY, readonly) BOOL updatePlace;
-(BOOL)removePlace:(int)index;
-(void)invalidate;

@end
