//
//  LibraryAPI.m
//  LocateMe
//
//  Created by Ye Ma on 2015-06-01.
//  Copyright (c) 2015 Ye Ma. All rights reserved.
//

#import "LibraryAPI.h"
#import "PersistencyManager.h"
#import "NSManagedObject+Place.h"

@interface LibraryAPI(){
    PersistencyManager *persistencyManager;
    onManagedObjectChange callbackAfterAddLocation;
    BOOL isOnline;
}

@end
@implementation LibraryAPI

+(LibraryAPI *)sharedInstance
{
    static LibraryAPI *sharedInstance;
    static dispatch_once_t token;
    dispatch_once(&token, ^{
        sharedInstance = [[LibraryAPI alloc] init];
    });

    return sharedInstance;
    
}

-(instancetype)init
{
    if (self = [super init]) {
        persistencyManager = [[PersistencyManager alloc] init];
    }
    return self;
}

-(void)addPlace:(onManagedObjectChange)callback
{
    CLLocation *lastKnowLocation = [[LocationManager sharedInstance] lastKnowLocation];
    
    if (lastKnowLocation == nil || [[LocationManager sharedInstance] lastLocationTooOld]) {
        callbackAfterAddLocation = callback;
        [[LocationManager sharedInstance] startLocationUpdateForObserver:self];
        return;
    }

    callback([persistencyManager addPlace]);
}

-(void)removePlace:(int)index callback:(onManagedObjectChange)callback
{
    callback([persistencyManager removePlace:index]);
}

-(void)updatePlace:(onManagedObjectChange)callback;
{
    callback([persistencyManager updatePlace]);
}

-(NSManagedObject *)getPlace:(int)index
{
    if (index < [self count]) {
        NSManagedObject *obj = persistencyManager.managedObjects[index];
        return obj;
    }
    return nil;
}

-(NSMutableArray *)getPlaces
{
    return persistencyManager.managedObjects;
}

- (NSMutableArray *)recentPlacesCloseby
{
    NSMutableArray *result = nil;
    if (persistencyManager.managedObjects && persistencyManager.managedObjects.count > 0)
    {
        result = [[NSMutableArray alloc] initWithCapacity:persistencyManager.managedObjects.count];
        NSManagedObject *lastAnnotation = (NSManagedObject *)[persistencyManager.managedObjects objectAtIndex:0];
        [persistencyManager.managedObjects enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            id<MKAnnotation> annotation = (id<MKAnnotation>)obj;
            CLLocationCoordinate2D location = [annotation coordinate];
            if ([lastAnnotation closeby:location])
            {
                [result addObject:obj];
            }
        }];
    }
    return result;
}

-(NSUInteger)count
{
    return persistencyManager.managedObjects.count;
}

-(void)setPredicateCondition:(NSPredicate *)pred
{
    [persistencyManager setPredicate:pred];
}

-(BOOL)isPredicateSearch
{
    return persistencyManager.isPredicateSearch;
}

-(BOOL)isNearbyPlace:(int)index
{
    __block BOOL isNearby = NO;
    NSManagedObject *objectForCurrentCell = [self getPlace:index];
    [persistencyManager.managedObjectsNearby enumerateObjectsUsingBlock:^(NSManagedObject *obj, NSUInteger idx, BOOL *stop) {
        //
        if ([obj isEqual:objectForCurrentCell]) {
            *stop = YES;
            isNearby = YES;
        }
        
    }];
    return isNearby;
}

-(void)invalidate
{
    [persistencyManager invalidate];
}


#pragma mark - Location Manager Delegate Methods

- (void)locationUpdated:(BOOL)failed
{
    if (!failed && callbackAfterAddLocation != nil) {
        [self addPlace:callbackAfterAddLocation];
        callbackAfterAddLocation = nil;
    }
    
    [[LocationManager sharedInstance] stopLocationUpdateForObserver:self];
}



@end
