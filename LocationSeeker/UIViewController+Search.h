//
//  UIViewController+Search.h
//  LocateMe
//
//  Created by Ye Ma on 2015-06-04.
//  Copyright (c) 2015 Ye Ma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Search)<UIAlertViewDelegate>

@end
