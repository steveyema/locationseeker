//
//  NSManagedObject+Place.h
//  LocateMe
//
//  Created by Ye Ma on 2015-06-01.
//  Copyright (c) 2015 Ye Ma. All rights reserved.
//

#import <CoreData/CoreData.h>
#import <MapKit/MapKit.h>

@interface NSManagedObject (Place)<MKAnnotation>

@property (nonatomic, readonly, assign) double lat;
@property (nonatomic, readonly, assign) double lon;
@property (nonatomic, copy) NSString *address;
@property (nonatomic, readonly, copy) NSDate *dateTime;
- (BOOL)closeby:(CLLocationCoordinate2D)location;

@end
