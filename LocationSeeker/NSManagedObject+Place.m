//
//  NSManagedObject+Place.m
//  LocateMe
//
//  Created by Ye Ma on 2015-06-01.
//  Copyright (c) 2015 Ye Ma. All rights reserved.
//

#import "NSManagedObject+Place.h"

static const double MaxDistance = 10000;

@implementation NSManagedObject (Place)

-(double)lat
{
    return [[self valueForKey:@"latitude"] doubleValue];
}

-(double)lon
{
    return [[self valueForKey:@"longitude"] doubleValue];
}

-(NSString *)address
{
    NSString *name = [self valueForKey:@"placemark"];
    if (name && name.length > 0)
    return [self valueForKey:@"placemark"];
    else return @"No name";
}

-(void)setAddress:(NSString *)address
{
    [self setValue:address forKey:@"placemark"];
}

-(NSDate *)dateTime
{
    return [self valueForKey:@"time"];
}

-(NSString *)description
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"YYYY-MM-dd"];
    return [NSString stringWithFormat:@"lat=%f,lon=%f,address=%@,date=%@", self.lat, self.lon, self.address,[formatter stringFromDate:[self dateTime]]];
}

-(BOOL)isEqual:(id)object
{
    if (self == object) return  YES;
    if ([self class] != [object class]) return NO;
    
    NSManagedObject *obj = (NSManagedObject *)object;
    return self.lat == obj.lat && self.lon == obj.lon && [self.address isEqualToString:obj.address] && [self.dateTime isEqualToDate:obj.dateTime];
}

- (BOOL)closeby:(CLLocationCoordinate2D)location
{
    double distance = [[[CLLocation alloc] initWithLatitude:self.lat longitude:self.lon] distanceFromLocation:[[CLLocation alloc] initWithLatitude:location.latitude longitude:location.longitude]];
    return distance <= MaxDistance;
}


#pragma mark MKAnnotation methods

-(CLLocationCoordinate2D) coordinate
{
    CLLocationCoordinate2D coord;
    
    coord.latitude = self.lat;
    coord.longitude = self.lon;
    
    return coord;
}

- (NSString *)title
{
    return self.address;
}

- (NSString *)subtitle
{
    
    return @"";
}

@end
