//
//  PersistencyManager.m
//  LocateMe
//
//  Created by Ye Ma on 2015-06-01.
//  Copyright (c) 2015 Ye Ma. All rights reserved.
//

#import "PersistencyManager.h"
#import "LocationManager.h"

@interface PersistencyManager(){
    BOOL reloadData;
    BOOL reloadNearbyData;
    NSManagedObjectModel *places;
}

@property (strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContextInMemory;
@property (strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinatorInMemory;
@property (assign, nonatomic, getter=isPredicateSearch) BOOL predicateSearch;

@end

@implementation PersistencyManager

@synthesize managedObjectModel = _managedObjectModel;
@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectContextInMemory = _managedObjectContextInMemory;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;
@synthesize persistentStoreCoordinatorInMemory = _persistentStoreCoordinatorInMemory;

@synthesize managedObjects = _managedObjects;
@synthesize managedObjectsNearby = _managedObjectsNearby;

-(instancetype)init
{
    if (self = [super init]) {
        reloadData = YES;
        reloadNearbyData = YES;
    }
    return self;
}

- (BOOL)addPlace
{
    NSManagedObjectContext *context = [self managedObjectContext];
    
    CLLocation *lastKnowLocation = [[LocationManager sharedInstance] lastKnowLocation];
    
    NSEntityDescription *newLocation = [NSEntityDescription insertNewObjectForEntityForName:@"Location" inManagedObjectContext:context];
    
    [newLocation setValue:@(lastKnowLocation.coordinate.latitude) forKey:@"latitude"];
    [newLocation setValue:@(lastKnowLocation.coordinate.longitude) forKey:@"longitude"];
    [newLocation setValue:lastKnowLocation.timestamp forKey:@"time"];
    
    NSArray *placemarks = [LocationManager sharedInstance].lastKnowPlacemarks;
    if (placemarks.count > 0) {
        CLPlacemark *placemark = (CLPlacemark *)placemarks[0];
        
        NSString *text = [NSString stringWithFormat:@"%@ %@\n%@ %@\n %@",
                          placemark.thoroughfare,
                          placemark.subThoroughfare,
                          placemark.postalCode,
                          placemark.locality,
                          placemark.country];
        
        [newLocation setValue:text forKey:@"placemark"];
    }
    
    NSError *error = nil;
    [context save:&error];
    
    if (error == nil) {
        reloadData = YES;
        reloadNearbyData = YES;
    }
    
    return error == nil;
}

-(BOOL)removePlace:(int)index
{
    NSManagedObjectContext *context = [self managedObjectContext];
    
    [context deleteObject:self.managedObjects[index]];
    NSError *error = nil;
    [context save:&error];
    
    if (error == nil) {
        reloadData = YES;
        reloadNearbyData = YES;
    }
    
    return error == nil;
    
}

-(BOOL)updatePlace
{
    NSManagedObjectContext *context = [self managedObjectContext];
    NSError *error = nil;
    [context save:&error];
    
    if (error == nil) {
        reloadData = YES;
        reloadNearbyData = YES;
    }
    
    return error == nil;
}

-(NSMutableArray *)reloadNearbyPlaces
{
    NSManagedObjectContext *context = [self managedObjectContextInMemory];
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"Location" inManagedObjectContext:context];
    
    //copy all records in sqlite to in-memory
    [self.managedObjects enumerateObjectsUsingBlock:^(id object, NSUInteger idx, BOOL *stop) {
        NSEntityDescription *newLocation = [NSEntityDescription insertNewObjectForEntityForName:@"Location" inManagedObjectContext:context];
        
        [newLocation setValue:[object valueForKey:@"latitude"] forKey:@"latitude"];
        [newLocation setValue:[object valueForKey:@"longitude"] forKey:@"longitude"];
        [newLocation setValue:[object valueForKey:@"time"] forKey:@"time"];
        [newLocation setValue:[object valueForKey:@"placemark"] forKey:@"placemark"];
        NSError *error = nil;
        [context save:&error];
    }];
    
    NSFetchRequest *request = [NSFetchRequest new];
    [request setEntity:entityDesc];
    
    NSPredicate *predicate = [NSPredicate predicateWithBlock:^BOOL(id location, NSDictionary *bindings) {
        NSLog(@"%@", location);
        double latitude = [[location valueForKey:@"latitude"] doubleValue];
        double longitude = [[location valueForKey:@"longitude"] doubleValue];
        CLLocation *loc = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
        CLLocation *currentLocation = [LocationManager sharedInstance].lastKnowLocation;
        double maxDistance = 200;
        double distance = [loc distanceFromLocation:currentLocation];
        NSLog(@"distance from current location: %f", distance);
        return  distance <= maxDistance;
    }];
    
    [request setPredicate:predicate];
    NSError *error = nil;
    NSArray *objects = [context executeFetchRequest:request error:&error];
    
    if (error == nil)
        return [NSMutableArray arrayWithArray:objects];
    
    return nil;
}

-(void)setPredicate:(NSPredicate *)predicate
{
    _predicate = predicate;
    _predicateSearch = predicate != nil;
    reloadData = YES;
    reloadNearbyData = YES;
}

- (NSMutableArray *)reloadPlaces
{
    NSManagedObjectContext *context = [self managedObjectContext];
    [context reset];
    
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"Location" inManagedObjectContext:context];
    
    NSFetchRequest *request = [NSFetchRequest new];
    [request setEntity:entityDesc];
    
    
    NSPredicate *pred = self.predicate? self.predicate : [NSPredicate predicateWithFormat:@"TRUEPREDICATE"];
    [request setPredicate:pred];

    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"time"
                                                                   ascending:NO];
    NSArray *sortDescriptors = @[sortDescriptor];
    [request setSortDescriptors:sortDescriptors];
    
    NSError *error;
    NSArray *objects = [context executeFetchRequest:request error:&error];
    
    if (error == nil) {
        return [NSMutableArray arrayWithArray:objects];
    }
    return nil;
}

-(NSMutableArray *)managedObjects
{
    if (reloadData) {
        reloadData = NO;
        
        _managedObjects  = [self reloadPlaces];
    }
    
    return _managedObjects;
}

-(NSMutableArray *)managedObjectsNearby
{
    if (reloadNearbyData) {
        reloadNearbyData = NO;
        
        _managedObjectsNearby  = [self reloadNearbyPlaces];
    }
    
    return _managedObjectsNearby;
}

-(void)invalidate
{
    reloadData = YES;
}

//helper funcation
- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "com.ma.steve.LocationSeeker" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

#pragma mark - CoreData stack methods
- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"LocationSeeker" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}


- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"LocationSeeker.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinatorInMemory {
    // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinatorInMemory != nil) {
        return _persistentStoreCoordinatorInMemory;
    }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinatorInMemory = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_persistentStoreCoordinatorInMemory addPersistentStoreWithType:NSInMemoryStoreType configuration:nil URL:nil options:nil error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinatorInMemory;
}


- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] init];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

- (NSManagedObjectContext *)managedObjectContextInMemory
{
    if (_managedObjectContextInMemory != nil) {
        return _managedObjectContextInMemory;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinatorInMemory];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContextInMemory = [[NSManagedObjectContext alloc] init];
    [_managedObjectContextInMemory setPersistentStoreCoordinator:coordinator];
    return _managedObjectContextInMemory;
}

@end
