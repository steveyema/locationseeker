//
//  tableViewController.m
//  LocationSeeker
//
//  Created by Ye Ma on 2015-01-02.
//  Copyright (c) 2015 Ye Ma. All rights reserved.
//

#import "ListViewController.h"
#import "AppDelegate.h"
#import "EditViewController.h"
#import "MapViewController.h"
#import "LibraryAPI.h"
#import "NSManagedObject+Place.h"
#import "UIViewController+Search.h"

#define HEADER_HEIGHT 45

@interface ListViewController () {
}

@property (nonatomic, strong) NSMutableArray *groupedObjects;
@end

@implementation ListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.groupedObjects = [NSMutableArray new];
    
    [self groupDates];

    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)removeLocation:(NSIndexPath *)indexPath
{
    [[LibraryAPI sharedInstance] removePlace:[self absoluteIndexFor:indexPath] callback:^(BOOL success) {
        self.groupedObjects = [NSMutableArray new];
        [self groupDates];
        [self.tableView reloadData];
    }];
}

- (IBAction)add:(id)sender {
    [[LibraryAPI sharedInstance] addPlace:^(BOOL success) {
        if (success) {
            self.groupedObjects = [NSMutableArray new];
            [self groupDates];
            [self.tableView reloadData];
        }
    }];
}

- (BOOL)isSameDayWithDateOne:(NSDate *)dateOne dateTwo:(NSDate *)dateTwo{
    
    NSCalendar *calender = [NSCalendar currentCalendar];
    
    unsigned unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay;
    
    NSDateComponents *compOne = [calender components:unitFlags fromDate:dateOne];
    NSDateComponents *compTwo = [calender components:unitFlags fromDate:dateTwo];
    
    return ([compOne day] == [compTwo day] && [compOne month] == [compTwo month] && [compOne year] == [compTwo year]);
}

- (NSMutableArray *)groupFor:(NSDate *)date
{
    for (NSMutableArray *array in self.groupedObjects) {
        for (NSManagedObject *object in array) {
            if ([self isSameDayWithDateOne:object.dateTime dateTwo:date]) {
                return array;
            }
        }
    }
    NSMutableArray *array = [NSMutableArray new];
    [self.groupedObjects addObject:array];
    return array;
}

- (void)groupDates
{
    self.groupedObjects = [NSMutableArray new];
    for (int i = 0; i < [LibraryAPI sharedInstance].count; i++) {
        NSManagedObject *obj = [[LibraryAPI sharedInstance] getPlace:i];
        [[self groupFor:obj.dateTime] addObject:obj];
    }
}

- (int)absoluteIndexFor:(NSIndexPath *)indexPath
{
    int index = 0;
    for (int i = 0; i < indexPath.section; i++) {
        index += ((NSArray *)self.groupedObjects[i]).count;
    }
    index += indexPath.row;
    return index;
}

- (BOOL)canBecomeFirstResponder
{
    return YES;
}

- (void)reloadData
{
    [self groupDates];
    [self.tableView reloadData];
}

- (BOOL)isNearbyCell:(NSIndexPath *)indexPath
{
    return [[LibraryAPI sharedInstance] isNearbyPlace:[self absoluteIndexFor:indexPath]];
}

- (NSDate *)time:(NSIndexPath *)indexPath
{
    return [[LibraryAPI sharedInstance] getPlace:[self absoluteIndexFor:indexPath]].dateTime;
}

- (NSString *)placemark:(NSIndexPath *)indexPath
{
    return [[LibraryAPI sharedInstance] getPlace:[self absoluteIndexFor:indexPath]].address;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return self.groupedObjects.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return ((NSMutableArray *)self.groupedObjects[section]).count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // Configure the cell...
    
    BOOL nearby = [self isNearbyCell:indexPath];
    
    NSString *cellID = nearby? @"CellHighlighted" : @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"MMM dd, yyyy HH:mm"];
    
    NSString *dateString = [format stringFromDate:[self time:indexPath]];
    
    cell.textLabel.text = [NSString stringWithFormat:@"%@\n%@", dateString, [self placemark:indexPath]];
    
    return cell;
}

- (NSString *)formattedSectionHeaderFor:(NSInteger)section
{
    if (section < self.groupedObjects.count) {
        NSArray *array = self.groupedObjects[section];
        if (array != nil && array.count > 0) {
            NSDate *date = ((NSManagedObject *)array[0]).dateTime;

            NSDateFormatter *format = [[NSDateFormatter alloc] init];
            [format setDateFormat:@"MMM dd, yyyy"];
            
            NSString *dateString = [format stringFromDate:date];
            return dateString;
        }
    }
    return @"";
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, HEADER_HEIGHT)];
    view.backgroundColor = [UIColor colorWithRed:48.0/255.0f green:131.0/255.0f blue:251.0/255.0f alpha:1];
    CGRect rect = [view bounds];
    rect.origin.x = 15;
    UILabel *label = [[UILabel alloc] initWithFrame:rect];
    label.text = [NSString stringWithFormat:@"%@", [self formattedSectionHeaderFor:section]];
    label.textColor = [UIColor whiteColor];
    [view addSubview:label];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return HEADER_HEIGHT;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 106;
}

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [self removeLocation:indexPath];
        [self.tableView reloadData];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}



// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}



// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}

#pragma mark - ViewControllerDelegate methods

-(void)contentUpdated
{
    [self.tableView reloadData];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    UIViewController *uiviewController = [segue destinationViewController];
    if ([uiviewController.title isEqualToString:@"Edit"]) {

        EditViewController *vController = (EditViewController *)uiviewController;
        vController.delegate = self;
        NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
        vController.obj = [[LibraryAPI sharedInstance] getPlace:[self absoluteIndexFor:indexPath]];
        
    }
}


@end
