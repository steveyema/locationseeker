//
//  mapViewController.m
//  LocationSeeker
//
//  Created by Ye Ma on 2015-01-03.
//  Copyright (c) 2015 Ye Ma. All rights reserved.
//

#import "MapViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "LibraryAPI.h"
#import "NSManagedObject+Place.h"
#import "UIViewController+MapView.h"
#import "UIViewController+Search.h"

@interface MapViewController () {
}
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (strong, nonatomic)    NSMutableArray *arrayAnnotation;

@end

@implementation MapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self reloadData];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)reloadData
{
    if (self.arrayAnnotation) {
        [self.mapView removeAnnotations:self.arrayAnnotation];
        self.arrayAnnotation = nil;
    }
    
    self.arrayAnnotation = [LibraryAPI sharedInstance].recentPlacesCloseby;
    
    NSLog(@"map region changed");
    
    if (self.arrayAnnotation != nil && self.arrayAnnotation.count > 0) {
        [self.mapView showAnnotations:self.arrayAnnotation animated:YES];
    }
}

- (IBAction)addClicked:(id)sender {
    [self addLocation];
}

- (void)addLocation
{
    [[LibraryAPI sharedInstance] addPlace:^(BOOL success) {
        if (success) {
            [self reloadData];
        }
    }];
}

@end
