//
//  tableViewController.h
//  LocationSeeker
//
//  Created by Ye Ma on 2015-01-02.
//  Copyright (c) 2015 Ye Ma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EditViewController.h"

@interface ListViewController : UITableViewController<ViewControllerDelegate>

@end
