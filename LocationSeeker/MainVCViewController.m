//
//  MainVCViewController.m
//  LocateMe
//
//  Created by Ye Ma on 2015-05-24.
//  Copyright (c) 2015 Ye Ma. All rights reserved.
//

#import "MainVCViewController.h"

@interface MainVCViewController ()

@end

@implementation MainVCViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)configureLeftMenuButton:(UIButton *)button
{
    CGRect frame = button.frame;
    frame.origin = (CGPoint){0,0};
    frame.size = (CGSize){40,40};
    button.frame = frame;
//    [button setAccessibilityLabel:ACCESSIBILITY_SLIDING_MENU_BUTTON];
    
    [button setImage:[UIImage imageNamed:@"Menu-25"] forState:UIControlStateNormal];
}

- (NSString *)segueIdentifierForIndexPathInLeftMenu:(NSIndexPath *)indexPath
{
    NSString *identifier;
    switch (indexPath.row) {
        case 0:
            identifier = @"mapSegue";
            break;
            
        case 1:
            identifier = @"listSegue";
            break;
            
        default:
            break;
    }
    
    return identifier;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
