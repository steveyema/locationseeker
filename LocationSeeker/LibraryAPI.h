//
//  LibraryAPI.h
//  LocateMe
//
//  Created by Ye Ma on 2015-06-01.
//  Copyright (c) 2015 Ye Ma. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LocationManager.h"

typedef void (^onManagedObjectChange)(BOOL success);

@interface LibraryAPI : NSObject<LocationManagerDelegate>

+(LibraryAPI *)sharedInstance;
-(void)addPlace:(onManagedObjectChange)callback;
-(void)removePlace:(int)index callback:(onManagedObjectChange)callback;
-(NSManagedObject *)getPlace:(int)index;
@property (NS_NONATOMIC_IOSONLY, readonly) NSUInteger count;
-(BOOL)isNearbyPlace:(int)index;
-(void)invalidate;
@property (NS_NONATOMIC_IOSONLY, getter=getPlaces, readonly, copy) NSMutableArray *places;
-(void)setPredicateCondition:(NSPredicate *)pred;
@property (NS_NONATOMIC_IOSONLY, getter=isPredicateSearch, readonly) BOOL predicateSearch;
@property (NS_NONATOMIC_IOSONLY, readonly, copy) NSMutableArray *recentPlacesCloseby;

@end
